import fetch from "auth/FetchInterceptor";

const userService = {};

userService.getUser = function (params) {
  return fetch({
    url: "/admin/user/get-by-conditions",
    method: "get",
    params,
  });
};

export default userService;
