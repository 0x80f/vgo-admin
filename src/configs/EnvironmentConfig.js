const dev = {
  API_ENDPOINT_URL: 'http://localhost:9798/api/v1'
};

const prod = {
  API_ENDPOINT_URL: 'http://139.180.218.205:9798/api/v1'
};

const test = {
  API_ENDPOINT_URL: 'http://139.180.218.205:9798/api/v1'
};

const getEnv = () => {
	switch (process.env.NODE_ENV) {
		case 'development':
			return dev
		case 'production':
			return prod
		case 'test':
			return test
		default:
			break;
	}
}

export const env = getEnv()
