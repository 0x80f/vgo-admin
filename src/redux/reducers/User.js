import {
  GET_USER,
  GET_USER_SUCCESS,
  SHOW_USER_MESSAGE,
} from '../constants/User';

const initState = {
  loading: false,
  message: '',
  showMessage: false,
  redirect: '',
  userList: [],
};

const user = (state = initState, action) => {
  switch (action.type) {
    case GET_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        userList: action.payload,
      };
    case SHOW_USER_MESSAGE: 
			return {
				...state,
				message: action.message,
				showMessage: true,
				loading: false
			}
    default:
      return state;
  }
};

export default user;