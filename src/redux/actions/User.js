import {
  GET_USER,
  GET_USER_SUCCESS,
  SHOW_USER_MESSAGE,
} from '../constants/User';

export const getUser = (payload) => {
  return {
    type: GET_USER,
    payload,
  };
};

export const onReceivedUsers = (users) => {
  return {
    type: GET_USER_SUCCESS,
    payload: users,
  };
};

export const showUserMessage = (message) => {
  return {
    type: SHOW_USER_MESSAGE,
    message,
  };
};
