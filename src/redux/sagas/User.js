import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import { GET_USER } from '../constants/User';
import { onReceivedUsers, showUserMessage } from '../actions/User';

import UserService from 'services/UserService';

export function* getUserList() {
  yield takeEvery(GET_USER, function* ({ payload }) {
    // const { page, size } = payload;
    try {
      const response = yield call(UserService.getUser, payload);
      console.log('response: ' + JSON.stringify(response));
      if (response.success) {
        yield put(onReceivedUsers(response.data));
      } else {
        yield put(showUserMessage(response.errorMessage));
      }
    } catch (err) {
      yield put(showUserMessage(err));
    }
  });
}

export default function* rootSaga() {
  yield all([fork(getUserList)]);
}
